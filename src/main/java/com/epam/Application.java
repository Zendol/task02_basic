/**
 * com.epam is a group of bar utils for operating on odd and even numbers.
 * also build Fibonacci numbers.
 */
package com.epam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class for making all functions with numbers and interaction with user.
 */
final class Application {
    /**
     * Constructor.
     */
    private Application() {

    }

    /**
     * Entry point into the program.
     * @param args amount of input data
     * @throws IOException if there are the issues.
     */
    public static void main(final String[] args) throws IOException {
        final int nPercentage = 100;
        System.out.println("Enter a and b: ");
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(System.in));
        int nA = Integer.parseInt(bufferedReader.readLine());
        int nB = Integer.parseInt(bufferedReader.readLine());
        int sumOdd = 0;
        int sumEven = 0;
        for (int i = nA; i <= nB; i++) {
            if (i % 2 == 0) {
                sumEven += i;
                System.out.println(i);
            }
        }

        for (int i = nB; i >= nA; i--) {
            if (i % 2 != 0) {
                sumOdd += i;
                System.out.println(i);
            }
        }

        System.out.println("Sum of odd numbers " + sumOdd);
        System.out.println("Sum of even numbers " + sumEven);
        System.out.println("Enter number of set : ");
        int nSetNumber = Integer.parseInt(bufferedReader.readLine());
        int nBigOdd = 0, nBigEven = 0;
        int countOdd = 0, countEven = 0;
        for (int i = 0; i < nSetNumber; i++) {
            int nCurFibNumber = Fibonacci.getFibNumber(i);
            if (nCurFibNumber % 2 != 0) {
                nBigOdd = nCurFibNumber;
                countOdd++;
            }
            if (nCurFibNumber % 2 == 0) {
                nBigEven = nCurFibNumber;
                countEven++;
            }
        }
        System.out.println("The biggest odd number :" + nBigOdd);
        System.out.println("The biggest even number "
                + ": " + nBigEven);
        System.out.println("Percentage of odd numbers : "
                + (countOdd / (nSetNumber * 1.0)) * nPercentage);
        System.out.println("Percentage of even numbers : "
                + (countEven / (nSetNumber * 1.0)) * nPercentage);
    }
}
